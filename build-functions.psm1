<# Note: Requires Powershell 2 or above

# What does this script do? Provides functions that do the following

	* Cleans the directory ..\dist\ for your build output to be placed into
	* Restores nuget dependencies with the Restore command
	* Compiles with msbuild the sln file in the same directory as this script using the configured version of msbuild and sensible defaults
	* OctoPacks any projects with octopack installed included to the release directory
	* Nuget packs any projects containing a nuspec file
	* Executes any NUnit tests identified based on naming convention
	* Logs build and tests outputs to files for you
	* Throws errors if Build() or Test() fail
	* Reports progress to the console and teamcity nicely

# How to use this library to create your own build script?

	1. Place this script in same folder as your solutions .sln file
	2. Install-Package NUnit.ConsoleRunner in your tests project
	3. If you are using octopus deploy then Install-Package OctoPack in each project to be deployed (db, website etc)
	3. Copy the following into a new file build.ps1 and place that file alongside this file
	*************************************** build.ps1 ************************************************
		Import-Module "$PSScriptRoot\build-functions.psm1" -Force

		# Execute the build!

		$config = "Release"
		Invoke-Clean -Config $config
		# OR for dotnet core change to the line below
		# Invoke-CleanCore -Config $config

		Restore
		# OR for dotnet core change to the line below
		# Restore -UseDotNet
		
		Invoke-Build -Config $config
		# OR for dotnet core change to the line below
		# Invoke-BuildCore -Config $config

		Invoke-TestNunit -Assembly "*UnitTests.dll" -NUnitConsole "nunit3-console.exe" -Config $config
		# OR for dotnet core change to the line below
		# Invoke-TestCore -ProjectNameFilter "*UnitTest*" -Config $config
		
		Pack -Config $config

		# for dotnet core you need to publish each project
   		# Invoke-PublishCore -Project MyProject -Config $config

		ReportResults

		Publish-TCArtifacts

	***************************************    Ends    ************************************************
	4. Execute the script with powershell PS> .\build.ps1

 #> 
 Set-StrictMode -Version latest

 # Configurable variables default values (override in your build script):
	 $OutputDirectory     = "..\dist"
	 $InfoMessageColour   = "Magenta"
	 $ToolsPath 			 = "\tools\dotnet"
 
 # Build and test functions
 
 
	 # Taken from psake https://github.com/psake/psake
	 <#
	 .SYNOPSIS
	 This is a helper function that runs a scriptblock and checks the PS variable $lastexitcode
	 to see if an error occcured. If an error is detected then an exception is thrown.
	 This function allows you to run command-line programs without having to
	 explicitly check the $lastexitcode variable.
	 .EXAMPLE
	 exec { svn info $repository_trunk } "Error executing SVN. Please verify SVN command-line client is installed"
	 #>
	 Function Exec
	 {
		 [CmdletBinding()]
		 param(
			 [Parameter(Position=0,Mandatory=1)][scriptblock]$cmd,
			 [Parameter(Position=1,Mandatory=0)][string]$errorMessage = ("Error executing command {0}" -f $cmd)
		 )
		 $scriptExpanded =  $ExecutionContext.InvokeCommand.ExpandString($cmd).Trim()
		 InfoMessage "Executing command: $scriptExpanded"
 
		 & $cmd
 
		 if ($lastexitcode -ne 0) {
			 throw ("Exec failed with exit code ${$lastexitcode}:" + $errorMessage)
		 }
	 }
 
	 # pretty print messages
	 Function InfoMessage ([string] $message)
	 { 
		 Write-Host "$message`n" -ForegroundColor $infoMessageColour
	 }
 
	 Function WarnMessage ([string] $message)
	 { 
		 Write-Host "Warning: $message`n" -ForegroundColor "Yellow"
	 }		
	 
	 # finds the most recent file below this directory matching a pattern
	 Function GetMostRecentFileMatchingPath([string] $FilePathPattern, [switch] $IgnoreError) #e.g GetMostRecentFileMatchingPath("*.sln")
	 {
		 $file = Get-ChildItem -Path $PSScriptRoot -Recurse -Filter $filePathPattern | Sort LastWriteTime | Select -last 1
		 if($file -eq "" -or $file -eq $null){
			 if(!$IgnoreError){
			 throw "Unable to find a file in $SolutionFolder (or below) matching $filePathPattern"
			 }
			 return $null;
		 }
		 return $file
	 }
 
	 # find all the unique folders that contain a file matching a specification
	 Function GetFoldersContainingFilesThatMatch([string] $FilePattern, [string] $ExcludePattern)
	 {
		 $items = Get-ChildItem -Filter $FilePattern -Recurse `
			 | Select-Object -expandproperty FullName `
			 | Get-Unique `
			 | Where { $_ -NotMatch $ExcludePattern } `
			 | foreach { Split-Path $_ -Parent } 
 
		 return $items
	 }
 
	 Function Clean([string] $MSBuild = "${Env:ProgramFiles(x86)}\MSBuild\14.0\Bin\MSBuild.exe", [string] $Config = "Release")
	 {
	     WarnMessage "'Clean' function is deprecated and will be removed - use 'Invoke-Clean' or 'Invoke-CleanCore' instead"
	     Invoke-Clean -MSBuild $MSBuild -Config $Config
	 }
 
	 Function Invoke-Clean([string] $MSBuild = "(auto)", [string] $Config = "Release") 
	 {
		 if($MSBuild -eq "(auto)")
		 {
			 $MSBuild = Find-MsBuild
		 }		 
		 
		 InfoMessage "Clean step: Emptying $ReleaseDir"
		 $removed = Remove-Item -path $ReleaseDir -recurse -force -ErrorAction silentlycontinue
		 $new = New-Item -path $ReleaseDir -type directory -force -ErrorAction silentlycontinue
		 $ReleaseDir = Resolve-Path $ReleaseDir
 
		 $MsBuildCleanArgs = $PathToSln, "/t:clean", "/m", "/p:Configuration=$Config", "/noconsolelogger"
		 InfoMessage "MsBuild Clean: `n  $MsBuild $MsBuildCleanArgs"
		 & $MsBuild $MsBuildCleanArgs
	 }

	Function Invoke-CleanCore([string] $BuildCommand ="dotnet", [string] $BuildArguments ="clean", [string] $Config = "Release") 
	{
		$FinalMsBuildArgs = $BuildArguments, $PathToSln, "--configuration", $Config

		InfoMessage "Empty release folder: Emptying $ReleaseDir"
		$removed = Remove-Item -path $ReleaseDir -recurse -force -ErrorAction silentlycontinue
		$new = New-Item -path $ReleaseDir -type directory -force -ErrorAction silentlycontinue
		$ReleaseDir = Resolve-Path $ReleaseDir

		InfoMessage "Executing Clean: `n  $BuildCommand $FinalMsBuildArgs"

		& $BuildCommand $FinalMsBuildArgs
	}
 
	 # Try and find a recent version of msbuild
	 Function Find-MsBuild([int] $MaxVersion = 2017)
	 {
		 $agentPath = "$Env:programfiles (x86)\Microsoft Visual Studio\2017\BuildTools\MSBuild\15.0\Bin\msbuild.exe"
		 $devPath = "$Env:programfiles (x86)\Microsoft Visual Studio\2017\Enterprise\MSBuild\15.0\Bin\msbuild.exe"
		 $proPath = "$Env:programfiles (x86)\Microsoft Visual Studio\2017\Professional\MSBuild\15.0\Bin\msbuild.exe"
		 $communityPath = "$Env:programfiles (x86)\Microsoft Visual Studio\2017\Community\MSBuild\15.0\Bin\msbuild.exe"
		 $fallback2015Path = "${Env:ProgramFiles(x86)}\MSBuild\14.0\Bin\MSBuild.exe"
		 $fallback2013Path = "${Env:ProgramFiles(x86)}\MSBuild\12.0\Bin\MSBuild.exe"
		 $fallbackPath = "C:\Windows\Microsoft.NET\Framework\v4.0.30319"
		 
		 If ((2017 -le $MaxVersion) -And (Test-Path $agentPath)) { return $agentPath } 
		 If ((2017 -le $MaxVersion) -And (Test-Path $devPath)) { return $devPath } 
		 If ((2017 -le $MaxVersion) -And (Test-Path $proPath)) { return $proPath } 
		 If ((2017 -le $MaxVersion) -And (Test-Path $communityPath)) { return $communityPath } 
		 If ((2015 -le $MaxVersion) -And (Test-Path $fallback2015Path)) { return $fallback2015Path } 
		 If ((2013 -le $MaxVersion) -And (Test-Path $fallback2013Path)) { return $fallback2013Path } 
		 If (Test-Path $fallbackPath) { return $fallbackPath } 
		 
		 throw "Yikes - Unable to find msbuild"
	 }
 
	 # Return a path to nuget.exe. Tries the following locations in order:
	 # - find it in child folders such as Nuget.CommandLine/octopack packages folders
	 # - see if it is available on the path
	 # - try and download it from nuget.org to the packages folder 
	 # - throw error if it cannot be found
	 Function Find-Nuget([string] $Executable = "nuget")
	 {
		 $localNugetPath = (GetMostRecentFileMatchingPath $Executable -IgnoreError)
		 $NugetExePath = If($localNugetPath -ne $null) { $localNugetPath.FullName } Else {"nuget"}
 
		 # cant find it locally or path? then download
		 if ((Get-Command $NugetExePath -ErrorAction SilentlyContinue) -eq $null) 
		 { 
			 $sourceNugetExe = "https://dist.nuget.org/win-x86-commandline/latest/nuget.exe"
			 $NugetExePath = "$SolutionFolder\packages\nuget.exe"
 
			 WarnMessage "Unable to find nuget.exe in your PATH or in your project. Trying to get it from the web! Downloading $sourceNugetExe to $NugetExePath"
 
			 mkdir "packages" -Force | Out-Null
			 $result = Invoke-WebRequest $sourceNugetExe -OutFile $NugetExePath -PassThru -UseBasicParsing
 
			 if($result.StatusCode -ne 200) { 
				 throw "Unable to download nuget from the web so Run 'Install-Package Nuget.CommandLine' to fix"
			 } else {
				 InfoMessage "Downloaded nuget to $NugetExePath"
			 }
		 }
 
		 return $NugetExePath
	 }
 
	 Function Restore([switch] $UseDotnet)
	 {
		 if($UseDotnet) {
			 $NugetExePath = "dotnet"
		 } else {
			 $NugetExePath = Find-Nuget
		 }
 
		 $NugetArgs =  "restore", $SolutionFolder
					 
		 InfoMessage "Restore nuget packages: `n  $NugetExePath $NugetArgs" 
		 & $NugetExePath $NugetArgs
	 }
 
	 # use msbuild to compile the sln file
	 # to find msbuild in 2017+ consider using vswhere, see https://github.com/Microsoft/vswhere/wiki/Find-MSBuild
	 # or use %programfiles(x86)%\Microsoft Visual Studio\2017\BuildTools\MSBuild\15.0\Bin on agents and C:\Program Files (x86)\Microsoft Visual Studio\2017\Enterprise\MSBuild\15.0\Bin on dev
	 Function Build([string] $Config = "Release", [string] $MSBuild = "(auto)", [string] $MsBuildArgs ="") 
	 {
		 WarnMessage "'Build' function is deprecated and will be removed - use 'Invoke-Build' or 'Invoke-BuildCore' instead"
 
		 Invoke-Build -Config $config -MsBuild $MsBuild -MsBuildArgs $MsBuildArgs
	 }	
 
	 Function Invoke-Build([string] $MSBuild = "(auto)", [string] $MsBuildArgs ="", [string] $Config = "Release") 
	 {
		 if($MSBuild -eq "(auto)"){
			 $MSBuild = Find-MsBuild
		 }
 
		 InfoMessage "Build: Compiling $PathToSln in $Config configuration"
		 InfoMessage "Log file: $LogFile"
 
		 $UseOctopack = $false
		 If((GetMostRecentFileMatchingPath "OctoPack.Tasks.dll" -IgnoreError) -ne $null){
			 InfoMessage "Detected you have Octopack installed - Adding RunOctoPack=true to MsBuild parameters for automatic packing"
			 $UseOctopack = $true
		 }
 
		 $OctopackMsbuildParams = If($UseOctopack) { "/p:RunOctoPack=true;OctoPackPublishPackageToFileShare=$ReleaseDir;OctoPackPublishPackagesToTeamCity=false" } else { "" }
	 
		 $FinalMsBuildArgs = $MsBuildArgs, $PathToSln, "/p:Configuration=$config", "/p:OutputPath=$ReleaseDir\$ProjName", $OctopackMsbuildParams, "/t:build", "/noautorsp", "/ds", "/m", "/l:FileLogger,Microsoft.Build.Engine;logfile=$LogFile"
 
		 InfoMessage "Executing MsBuild: `n  $MSBuild $FinalMsBuildArgs"
 
		 & $MsBuild $FinalMsBuildArgs
	 }
 
	 Function Invoke-BuildCore([string] $BuildCommand ="dotnet", [string] $BuildArguments ="build", [string] $Config = "Release") 
	 {
		 InfoMessage "Using $BuildCommand $BuildArguments to compile $PathToSln in $Config configuration"
		 InfoMessage "Log file: $LogFile"
 
		 $FinalMsBuildArgs = $BuildArguments, $PathToSln, "--no-restore", "--configuration", $Config, "/p:AssemblyVersion=$BuildNumber", "/p:Version=$BuildNumber", "/flp:logfile=$LogFile"
 
		 InfoMessage "Executing Build: `n  $BuildCommand $FinalMsBuildArgs"
 
		 & $BuildCommand $FinalMsBuildArgs
	 }
 
	 # Execute 'dotnet publish' on a project and place the output and log in the release distibution folder
	 # E.g Invoke-Publish -Project MyApp
	 Function Invoke-PublishCore([string] $Project, [string] $BuildCommand ="dotnet", [string] $BuildArguments ="publish", [string] $Config = "Release") 
	 {
		 $ProjectPublishLog = "$ReleaseDir\$Project-Publish.log"
		 InfoMessage "Using $BuildCommand $BuildArguments to publish $Project in $Config configuration"
		 InfoMessage "Log file: $ProjectPublishLog"
 
		 $FinalDotnetArgs = $BuildArguments, "$SolutionFolder\$Project", "--output", "$ReleaseDir\$Project", "--no-restore", "--configuration", $Config, "/p:AssemblyVersion=$BuildNumber", "/p:Version=$BuildNumber", "/flp:logfile=$ProjectPublishLog"
 
		 InfoMessage "Executing Publish: `n  $BuildCommand $FinalDotnetArgs"
 
		 $result = & $BuildCommand $FinalDotnetArgs
 
		 if(Test-Path $ProjectPublishLog) {
			 Get-Content $ProjectPublishLog -OutVariable PublishResult | out-null
 
			 if ($PublishResult.IndexOf("Build succeeded.") -lt 0)
			 {
				 $failMessage =  "Publish $Project FAILED! See msbuild log: $ProjectPublishLog"
				 if($IsTeamcity){ Write-Host "##teamcity[buildStatus status='FAILURE' text='$failMessage']" }
				 throw $failMessage
			 } 
			 else{
				 InfoMessage "`nMsBuild publish log file reports success ($ProjectPublishLog)"
			 }
		 }
 
		 return $result
	 }
 
	 # execute any nunit tests identified by tests naming convention 
	 Function Test-NUnit([string] $Assembly = "*Tests.dll", [string] $NUnitConsole = "nunit3-console.exe", [string] $Config = "Release") 
	 {
		 WarnMessage "'Test-NUnit' function is deprecated and will be removed - use 'Invoke-TestNunit' for nunit3-console or 'Invoke-TestCore' for dotnet test instead"
		 Invoke-TestNUnit -Assembly $Assembly -NUnitConsole $NUnitConsole -Config $Config
	 }		
 
	 # Run NUnit tests using the nunit console test runner. Finds all tests DLLs matching a pattern and executes the nunit tests
	 # Will report results to Teamcity (if available) and outputs the test result file to the Release directory
	 # TODO: each call should produce different test file outpus ($NunitTestOutput) but at the moment they overwrite each other
	 Function Invoke-TestNUnit([string] $Assembly = "*Tests.dll", [string] $NUnitConsole = "nunit3-console.exe", [string] $Config = "Release") 
	 {
		 if($LASTEXITCODE -eq 0){
			 $NUnitConsolePath = (GetMostRecentFileMatchingPath $NUnitConsole -IgnoreError).FullName
 
			 if($NUnitConsolePath) {
				 InfoMessage "NUnit tests: Finding tests matching $Assembly in bin folders."
		 
				 # Find tests in child folders (except obj)
				 $TestDlls = Get-ChildItem -Path $PSScriptRoot -Recurse  -Include $Assembly | Select-Object -expandproperty FullName | Where-Object {$_ -NotLike "*\obj\*"} | % { "`"$_`"" }
 
				 if(@($TestDlls).Count -eq 0)  {
					 # Add the tests in the output folders (except obj)
					 $TestDlls += Get-ChildItem -Path $ReleaseDir -Recurse -Include $Assembly | Select-Object -expandproperty FullName | Where-Object {$_ -NotLike "*\obj\*"} | % { "`"$_`"" }
				 }
 
				 $teamcityOption = If($IsTeamcity) { "--teamcity" } Else { "" }
 
				 $NUnitArgs  =  ("/config:$Config", "/process:Multiple", "--result=TestResult.xml", $teamcityOption)
				 $NUnitArgs += $TestDlls
 
				 InfoMessage "Found $(@($TestDlls).Count) test DLL(s): $TestDlls. Test Output will save to $nunitTestOutput)"
				 InfoMessage "Executing Nunit: `n  $NUnitConsolePath $NUnitArgs     " 
 
				 if(@($TestDlls).Count -eq 0) 
				 {
					 WarnMessage "No tests found!"
				 } else {
					 & $NUnitConsolePath $nunitArgs 2>&1 # redirect stderr to stdout, otherwise a build with muted tests is reported as failed because of the stdout text
					 
					 InfoMessage "Placing test result file at $NunitTestOutput"
					 Move-Item -Path "TestResult.xml" -Destination $NunitTestOutput
				 }
			 } else {
				 InfoMessage "Skipping Test-NUnit() - no nunit console available"
			 }
		 }
		 else
		 {
			 InfoMessage "Skipping Test() - Build() probably failed or no nunit console available"
		 }
	 }
 
	 Function Get-AssemblyNameFromCsProj {
		 param (
			 [string] $ProjectFolder
		 )
		 
		 Try {
			 $csProjFile = (Get-ChildItem $ProjectFolder -Filter *.csproj)[0]
			 $AssemblyName = $csProjFile.BaseName
		 } Catch {
			 Throw "Unable to locate csproj file in '$ProjectFolder'"
		 }
 
		 $CsProjXml = ([xml](Get-Content $csProjFile.FullName)).Project
		 if(Get-Member -inputobject $CsProjXml.PropertyGroup -Name "AssemblyName" -MemberType Properties){
			 $AssemblyName = $CsProjXml.PropertyGroup.AssemblyName
		 }
		 $AssemblyName
	 }
 

	 <#
	 .SYNOPSIS
	 Discovers unit and integration tests and invokes a test CLI tool to execute them. Defaults to `dotnet test`
	 
	 .DESCRIPTION
	 Uses a folder naming convention to discover and execute your unit and integration test projects using a command line test executor such as `dotnet test`. Works well with nunit/xnuit and teamcity. You can use `Invoke-TestCore` in the solution root folder to discover the all the unit test projects automatically, and execute the all the tests using the dotnet CLI `dotnet test`. 
	 
	 By default `Invoke-TestCore` will:
	  * Localte all projects matching the pattern ProjectNameFilter and execute them sequentially
	  * Assume your project has already been restored/compiled (using `dotnet build`)
	  * Use sensible defaults, like using the dotnet test CLI in Release config but allow you to override them
	  * Output a code coverage report to the $ReleaseDir using coverlet
	  * Tell teamcity about your test coverage using Teamcity service messages
	  * Exclude projects named *Tests* from code coverage stats, and makes this configurable.
	
	 .PARAMETER TestCommand
	 The CLI test runner tool to execute. Defaults to `dotnet`
	 
	 .PARAMETER TestArguments
	 The CLI test runner tool arguments to be passed to TestCommand. Defaults to `test`
	 
	 .PARAMETER Config
	 The build configuration, usually Debug or Release. Defaults to Release.
	 
	 .PARAMETER ProjectNameFilter
	 A Get-ChildItem filter to be used to locate the folders that contain test projects. Defaults to *UnitTest* and would match folders named MyApp.MyProj.UnitTests or MyApp.UnitTest
	 
	 .PARAMETER SkipCodeCoverage
	 A bool switch to disable coverlet code coverage stats being generated. Defaults to False.
	 
	 .PARAMETER CoverletExcludes
	  Specify a string to define assembly or project exclusions that are passed to coverlet (https://github.com/tonerdo/coverlet). 
	  Defaults to "[*.*Tests?]*" which excludes assembly names containing 'Test' or 'Tests' from code coverage reports since we usually don't want the coverage of the classes in a unit test project inflating production code coverage stats. Also accepts an array such as:
	   "[*.UnitTests]*","[*.IntegrationTests]*"
	 
	 .EXAMPLE
	 Given a project MyApp.dll with a Unit Test project MyApp.UnitTests.dll you can execute the following Powershell in the solution root to locate the unit tests and execute them and output code coverage stats:

	 PS$> Invoke-TestCore

	 .EXAMPLE
	 Given a project MyApp.dll with a Test project MyApp.IntegrationTests.dll built in debug configuration you can execute the following Powershell in the solution root to locate the unit tests and execute them and output code coverage stats:

	 PS$> Invoke-TestCore -Config Debug -ProjectNameFilter "*.IntegrationTests"

	 .EXAMPLE
	 Given a project MyApp.dll with a Test project MyApp.IntegrationTests.dll built in debug configuration you can execute the following Powershell in the solution root to locate the unit tests and execute them and output code coverage stats:

	 PS$> Invoke-TestCore -Config Debug -ProjectNameFilter "*.IntegrationTests"

	 .NOTES
     Typically for each test project you need to have the following:
	
	 	$PS> dotnet add package Microsoft.NET.Test.Sdk
		$PS> dotnet add package NUnit   
		$PS> dotnet add package NUnit3TestAdapter
		$PS> dotnet add package TeamCity.VSTest.TestAdapter
	 #>

	 Function Invoke-TestCore(
		 [string] $TestCommand ="dotnet", 
		 [string] $TestArguments ="test", 
		 [string] $Config = "Release", 
		 [string] $ProjectNameFilter = "*UnitTest*",
		 [switch] $SkipCodeCoverage = $false,		
		 [string[]] $CoverletExcludes = "[*.*Tests?]*"
		 ) 
	 {
		 # consider dotnet vstest (Get-ChildItem -recurse -File *.Tests.*dll | ? { $_.FullName -notmatch "\\obj\\?" }) for agregated test run
 
		 if($LASTEXITCODE -eq 0){
			 $CoverageDataFolder = Join-Path (Resolve-Path $ReleaseDir) "\Coverage"
 
			 if(-Not($SkipCodeCoverage))
			 {
				 # Get code coverage dependencies
				 Install-Tool -InstalledExePath reportgenerator.exe -ToolName dotnet-reportgenerator-globaltool
				 Install-Tool -InstalledExePath coverlet.exe -ToolName coverlet.console
			 }
 
			 if((Test-Path $TestCommand) -Or (Get-Command $TestCommand -ErrorAction SilentlyContinue) -ne $null) {
				 
				 InfoMessage "Tests: Finding tests matching $ProjectNameFilter"
 
				 $TestFolders = Get-ChildItem -filter $ProjectNameFilter -Directory -Recurse -Path $PSScriptRoot
				 $ReporterFolder = (Resolve-Path $ReleaseDir)
				 
				 InfoMessage "Found $(@($TestFolders).Count) test folders: $TestFolders."
 
				 $TestFolders | ForEach-Object {
					 $TestFolder = $_.FullName
					 $FinalTestArgs = $TestArguments, $TestFolder, "-c:$Config", "-r:$ReporterFolder", "--no-build", "--no-restore"
 
					 if($SkipCodeCoverage){
						 InfoMessage "Executing Test: `n $TestCommand $FinalTestArgs"
						 & $TestCommand $FinalTestArgs 
					 } 
					 else {
						 $AssemblyName = Get-AssemblyNameFromCsProj -ProjectFolder $_.FullName
						 $AssemblyPath = Get-ChildItem -Path $TestFolder\bin\$Config -Recurse -Include "$AssemblyName.dll"
						 $CoverletTargetArgs = ($FinalTestArgs -Join " ")
						 InfoMessage "Instrumenting $AssemblyPath for code coverage before testing"

						 $command = "coverlet"
						 $commandArgs = [System.Collections.ArrayList] ($AssemblyPath, `
							"--target", $TestCommand, `
							"--targetargs", $CoverletTargetArgs, `
							"--merge-with", "$CoverageDataFolder\coverage.json", `
							"--format","json", "--format", "opencover", "--output", "$CoverageDataFolder\")

						foreach($CoverletExclude in $CoverletExcludes){
							$commandArgs.Add("--exclude") | Out-Null
							$commandArgs.Add($CoverletExclude) | Out-Null
						}

						if($IsTeamcity){ 
							$commandArgs.Add("--format") | Out-Null
							$commandArgs.Add("teamcity") | Out-Null
						}

						Exec { & $command $commandArgs }
					 }
				 }
 
				 if(-Not($SkipCodeCoverage))
				 {
					 Write-Host "Generating coverage html report to $CoverageDataFolder"
					 reportgenerator -reports:$CoverageDataFolder\coverage.opencover.xml -targetdir:$CoverageDataFolder\ -reporttypes HTML #-assemblyfilters:"-nunit*;-*.test.dll;-*.tests.dll"
 
					 if($IsTeamcity)
					 { 
					 	Write-Host "Publishing coverage.zip to $ReleaseDir"
						Compress-Archive -Path $CoverageDataFolder\* -DestinationPath $ReleaseDir\Coverage.zip
						Remove-Item -path $CoverageDataFolder -recurse -force
					 }
				 }
				 
				 if(@($TestFolders).Count -eq 0) 
				 {
					 WarnMessage "No tests found!"
				 }
			 } else {
				 InfoMessage "Skipping Test-Core() - could not find $TestCommand"
			 }
		 }
		 else
		 {
			 InfoMessage "Skipping Test-Core() - Invoke-BuildCore() probably failed or no test command was available"
		 }
	 }
 
	 # find folders with nuspec files and pack them using nuget
	 Function Pack([string] $Executable = "nuget.exe", [string] $Config = "Release")
	 {
		 if($LASTEXITCODE -eq 0)
		 {
			 # use nuget.exe from package Nuget.CommandLine/octopack if possible, else if it is on path, use that
			 $NugetExePath = Find-Nuget $Executable
 
			 $FoldersWithNuspecs = GetFoldersContainingFilesThatMatch "*.nuspec" "(packages)|(obj)"
			 ForEach($projectFolder In $FoldersWithNuspecs)
			 {
				 # assuming here that we want to ensure the folder has a csproj
				 If(Test-Path (Join-Path $projectFolder "*.csproj")){
					 $projectToPack = Join-Path $projectFolder "*.csproj" -Resolve
					 $NugetArgs =  "Pack", "$projectToPack", "-Properties", "Configuration=$Config", "-OutputDirectory", "$ReleaseDir"
					 
					 InfoMessage "Executing pack on $projectToPack `n   $NugetExePath $NugetArgs" 
					 & $NugetExePath $NugetArgs
				 }
			 }
 
			 if(@($FoldersWithNuspecs).Count -eq 0) 
			 {
				 InfoMessage "Skipping Pack() step - no *.nuspec files found to Pack"
			 }
		 } 
		 else
		 {
			 InfoMessage "Skipping Pack() - previous step failed - probably Test()"
		 }
	 }
 
	 <#
	 .SYNOPSIS
	 Install a new CLI tool using dotnet tool install
 
	 .PARAMETER InstalledExePath
	 Once installed, what is the relative path from ToolsFolder to the executable, e.g. "dotnet-octo.exe" 
	 
	 .PARAMETER ToolName
	 nuegt package name for the dotnet tool, e.g. "dotnet-octo"
	 
	 .PARAMETER ToolsFolder
	 Full path of folder to install tool into. Defaults to "c:\tools\dotnet"
	 
	 .PARAMETER DaysBeforeUpdate
	 How many days since the last dotnet tool install/update before forcing a tool update from source
	 
	 .EXAMPLE
	 Install-Tool -InstalledExePath coverlet.exe -ToolName coverlet.console
	 
	 #>
	 Function Install-Tool
	 {
		 Param
		 (
			 [Parameter(Mandatory=$true)]
			 [string] $InstalledExePath,
			 [Parameter(Mandatory=$true)]
			 [string] $ToolName,
			 [string] $ToolsFolder = "\tools\dotnet",
			 [int]    $DaysBeforeUpdate = 30
		 )
	 
		 $toolPath   = "$ToolsFolder\$installedExePath"
		 $lastUpdate = (Get-Item $toolPath -ErrorAction SilentlyContinue)
		 $command    = if($lastUpdate) { "update" } else { "install" }
		 $needUpdate = $lastUpdate -and (((Get-Date) - $lastUpdate.LastWriteTime) -gt (New-timespan -days $DaysBeforeUpdate))
 
		 if(($command -eq "install") -or $needUpdate) {
			 Exec {
				 dotnet tool $command $ToolName --tool-path $ToolsFolder
			 }
		 } else {
			 $lastWrite = $lastUpdate.LastWriteTime.ToString("dd/MM/yyyy")
			 InfoMessage "Skipping tool update for $toolPath as last update ($lastWrite) was less than $DaysBeforeUpdate days ago"
		 }
 
		 if(-not (Test-Path $toolPath)) {
			 throw "Unable to find tool installed at $toolPath - Install probably failed. Check params InstalledExePath and ToolName"
		 }
		 
		 $ToolsFolderPattern = "$ToolsFolder;".Replace("\", "\\")
		 If (-Not ($env:PATH -match $ToolsFolderPattern )) {
			 # Add the tools path to the PATH env variable
			 InfoMessage "Adding $ToolsFolder to your PATH"
			 $env:PATH = "$ToolsFolder;" + $env:PATH
		 }
	 }
 
	 <#
	 .SYNOPSIS
	 Installs dotnet octo CLI tool if not available and adds it to your path for the rest of your session. Will do an update if octo is out of date.
	 
	 .DESCRIPTION
	 Default install location is c:\tools\dotnet which is added to your PATH. once this is run you can run `dotnet octo`
	 
	 .EXAMPLE
	 Install-Octo
	 #>
	 Function Install-Octo() {
		 Install-Tool -InstalledExePath dotnet-octo.exe -ToolName Octopus.DotNet.Cli
	 }
	 
	 <#
	 .SYNOPSIS
	 Creates a zip file of a directory containing terraform files ready for deployment
	 
	 .DESCRIPTION
	 Creates a zip file of a directorys terraform files and outputs it to a parent directory called dist ready for deployment. This package can then be sent to octopus deploy
	 
	 .PARAMETER id
	 The ID of the package to create
	 
	 .PARAMETER version
	 The Semver version number of the package to create
	 
	 .PARAMETER FolderToZip
	 The folder to be zipped
	 
	 .PARAMETER outputFolder
	 Where to place the new zip file
	 
	 .PARAMETER Include
	 pattern for files to be included in zip by default. Defaults to *.*. For more an one comma seperate.
	 
	 .PARAMETER OctoPackArgs
	 Additional args to be passed to dotnet octo. For more an one comma seperate.

	 .PARAMETER RemoveFolder
	 Should the source FolderToZip be removed after sucessful zipping?
	 
	 .EXAMPLE
	 New-ZipPackage -id "hello-world" -version "1.0.0.0" -FolderToZip "c:\proj\hello\src" -outputFolder "c:\proj\hello\dist" -Include "*.tf","*.tfvars"
	 
	 .NOTES
	 
	 #>
	 Function New-ZipPackage([String] $Id, [String] $Version, [String] $FolderToZip, [String] $OutputFolder, [String[]] $Include = "**\*.*", [String[]] $OctoPackArgs = "", [switch] $RemoveFolder = $false) {
 
		 $format = "zip"
		 $FolderToZip = Resolve-Path $FolderToZip
		 $OutputFolder = Resolve-Path $OutputFolder
 
		 InfoMessage "Zipping files in '$FolderToZip'"
		 
		 # Build a cli command to pack the files into a zip with version number from Teamcity
		 $command = "dotnet"
		 $commandArgs = [System.Collections.ArrayList] ("octo", "pack", "--id=$Id", "--version=$Version", "--basePath=$FolderToZip", "--outFolder=$OutputFolder", "--format=$format")
 
		 foreach($includePattern in $Include){
			 $commandArgs.Add("--include=$IncludePattern") | Out-Null
		 }
		 foreach($octoPackArg in $OctoPackArgs){
			 $commandArgs.Add($octoPackArg) | Out-Null
		 }
 
		 InfoMessage "Pack command:`n    $command $commandArgs"
		 $result = & $command $commandArgs
 
		 if($LastExitCode -ne 0) {
			 throw "Error during pack. $result"
		 } 
		 else 
		 {
			  $result | Write-Host

			  # Optionally, and only on sucess, delete the source folder
			  if($RemoveFolder)
			  {
				  Remove-Item $FolderToZip -Recurse -Force -ErrorAction SilentlyContinue | Out-Null
			  }
		 }
		 # Return path to created package
		 $zipPath = (Get-ChildItem "$outputFolder\$id.$version.$format" | Select-Object -First 1).FullName
 
		 InfoMessage "Zipped files to $zipPath"
 
		 return $ZipPath
	 }
 
 
	 # raise appropriate failure errors or report sucess
	 Function ReportResults()
	 {
		 $failMessage = ""
 
		 if(Test-Path $LogFile) {
			 Get-Content $LogFile -OutVariable BuildSolutionResult | out-null
 
			 if ($BuildSolutionResult.IndexOf("Build succeeded.") -lt 0)
			 {
				 $failMessage = "MsBuild FAILED! See msbuild log: $LogFile "
			 } 
			 else{
				 InfoMessage "`nMsBuild log file reports success ($LogFile)"
			 }
		 }
 
		 # Iterate through any .trx test result files in the relase folder and check the outcome
		 # Get-ChildItem -Path $ReleaseDir -Filter *.trx | ForEach-Object {
		 # 	Get-Content $_.FullName -OutVariable testResult | out-null
		 # 	if($testResult -match "<ResultSummary outcome=`"Failed`">" -and $failMessage -eq ""){
		 # 		$failMessage = "Tests FAILED! See " + $_.FullName				
		 # 	}
		 # }
 
		 # check the NUnit tests file for failures
		 if(Test-Path $nunitTestOutput) 
		 {
			 Get-Content $nunitTestOutput -OutVariable testResult | out-null
 
			 if ($testResult -match 'result="Failed"' -and $failMessage -eq "")
			 {
				 $failMessage = "Tests FAILED! See $nunitTestOutput "
			 }
			 else{
				 InfoMessage "`nNUnit log file reports success ($nunitTestOutput)"
			 }
		 }
 
		 if($failMessage -ne ""){
			 if($IsTeamcity){ Write-Host "##teamcity[buildStatus status='FAILURE' text='$failMessage']" }
			 throw $failMessage
		 }
		 else
		 {
			 $duration = $stopwatch.Elapsed.TotalSeconds
			 InfoMessage "`nSuccess! Steps completed successfully in $duration s"
		 }
	 }
 
	 <#
	 .SYNOPSIS
	 When running under teamcity, inform it of a folder to be considered as build artifacts
	 
	 .PARAMETER folder
	 folder path to be published
	 
	 .EXAMPLE
	 Publish-TCArtifacts "c:\proj\hello\dist"
	 #>
	 Function Publish-TCArtifacts([String] $folder){
		 If ($IsTeamcity -and (Test-Path $folder)) { 
			 Write-Host "##teamcity[publishArtifacts '$folder']" 
		 } else {
			 Write-Host "Teamcity Artifact publish skipped" -ForegroundColor Yellow
		 }
	 }
 
	 <#
	 .SYNOPSIS
	 Push a package to an Octopus Deploy Server
	 
	 .DESCRIPTION
	 If you have env variables set fopr env:\Octopus_Url and env:\Octopus_ApiKey then push the specified package to that server using the key
	 
	 .PARAMETER packagePath
	 path of the .zip or .nupkg to be published
	 
	 .EXAMPLE
	 Publish-ToOctopusGallery "c:\proj\hello\dist\hello.1.0.0.0.zip"
	 #>
	 Function Publish-ToOctopusGallery ([String] $packagePath) {
		 If (($IsTeamcity) `
		 -and (Test-Path env:\Octopus_Url) `
		 -and (Test-Path env:\Octopus_ApiKey)) {
			 Write-Host "Publishing $packagePath to $env:Octopus_Url"
			 dotnet octo push --package $packagePath --server $env:Octopus_Url --apiKey  $env:Octopus_ApiKey
 
			 if($LastExitCode -ne 0) {
				 throw "Error pushing package to Octopus"
			 }
		 }
		 else {
			 Write-Host "Octopus publish skipped" -ForegroundColor Yellow
		 }
	 }

	 Function Publish-ToNugetGallery ([String] $packagePath) {

		Write-Host "Ready to publish $packagePath (On branch $BranchName)"
	
		If (($IsTeamcity) `
				-and (Test-Path env:\Nuget_Url) `
				-and (Test-Path env:\Nuget_ApiKey) `
				-and $BranchName -eq "master") {
			
			$nugetExePath = Find-Nuget "nuget.exe"
			$nugetArgs = "push", "$packagePath", "-Source", $env:Nuget_Url, "-ApiKey", $env:Nuget_ApiKey
			
			& $nugetExePath $nugetArgs -NoNewWindow -Wait
	
		} else{
			Write-Host "Publish to nuget skipped as missing Nuget_Url, Nuget_ApiKey or current branch != master" -ForegroundColor Yellow
		}
	}
 
 <# Computed and other variables #> 
	 $PSScriptRoot          = If($PSScriptRoot -eq $null) { Split-Path $MyInvocation.MyCommand.Path -Parent } else { $PSScriptRoot }
	 $SolutionFolder        = (Get-Item -Path $PSScriptRoot -Verbose).FullName
	 $ProjName              = try {(GetMostRecentFileMatchingPath "*.sln").Name.Replace(".sln", "") } catch {}
	 $PathToSln             = "$SolutionFolder\$ProjName.sln"
	 $ReleaseDir            = "$SolutionFolder\$OutputDirectory"
	 $LogFile               = "$ReleaseDir\$ProjName-Build.log"
	 $NunitTestOutput       = "$ReleaseDir\TestResult.xml"
	 $Stopwatch             = [Diagnostics.Stopwatch]::StartNew()
	 $IsTeamcity            = If(Test-Path env:\TEAMCITY_VERSION) { $true } Else { $false }
	 $BuildNumber 		    = if (Test-Path env:\build_number) { $env:build_number } Else { "1.0.0.0" }
	 $BranchName            = &git rev-parse --abbrev-ref HEAD 
 
 Write-host "Info: Running under Powershell version: " + $PSVersionTable.PSVersion
 
 Export-ModuleMember -Variable OutputDirectory,ToolsPath,SolutionFolder,ProjName,PathToSln,ReleaseDir,IsTeamcity,BuildNumber,BranchName -Function InfoMessage,WarnMessage,GetMostRecentFileMatchingPath,GetFoldersContainingFilesThatMatch,Clean,Find-Nuget,Find-MsBuild,Restore,Build,Invoke-Build,Invoke-BuildCore,Invoke-PublishCore,Test-NUnit,Invoke-TestNUnit,Invoke-TestCore,Pack,New-ZipPackage,Install-Octo,ReportResults,Publish-TCArtifacts,Publish-ToOctopusGallery,Invoke-Clean,Invoke-CleanCore