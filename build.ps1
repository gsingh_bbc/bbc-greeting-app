Import-Module -Name $PSScriptRoot\build-functions.psm1 -Force

$config = "Release"
$packageId = "BBC.Greeting.API"

## below select either full framework or dotnet core section and delete the other section ##

<########################### .net full framework version  #######################

# Execute the build in stages - Clean > Restore > Compile > Test > Pack > Reporting
Clean -Config $config

# Restore Dependencies
Restore 

# Compile the app (and package with octopack if needed)
Invoke-Build -Config $config

# Run unit tests
Invoke-TestNunit -Assembly "*UnitTests.dll" -NUnitConsole "nunit3-console.exe" -Config $config

# Pack any projects with a nuspec using nuget
Pack -Config $config

ReportResults

Publish-TCArtifacts $ReleaseDir

# send nupkg to the octopus server
Publish-ToOctopusGallery "$ReleaseDir\$packageId.$BuildNumber.nupkg"

########################## End .net full framework ########################>


 ######################### .net core version version ######################
  
Import-Module -Name $PSScriptRoot\build-functions.psm1 -Force

$project = "BBC.Greeting.API"
$packageId = "BBC.Greeting.API"
$publishedFolder = "$ReleaseDir\$project"

Install-Octo

# Execute the build in stages - Clean > Restore > Compile > Test > Pack > Reporting
Clean -Config $config

# Restore Dependencies
Restore -UseDotNet

# Compile the app
Invoke-BuildCore -Config $config

# Run unit tests - none ATM!
Invoke-TestCore -ProjectNameFilter "BBC.Greeting.API.Test" -Config $config

# Package up any terraform scripts
$infrastructurePackage = New-ZipPackage -Id "$packageId.Infrastructure" -Version $BuildNumber -FolderToZip "$PSScriptRoot\infrastructure" -Include "*.tf","*.tfvars" -OutputFolder $ReleaseDir 

# publish web or api project, zip it up and remove the unzipped output
Invoke-PublishCore -Project $project -Config $config
$webAppPackage = New-ZipPackage -id "$packageId.Web" -version $BuildNumber -FolderToZip $publishedFolder -outputFolder $ReleaseDir -RemoveFolder

ReportResults

Publish-TCArtifacts $ReleaseDir

# Push the .zip to the octopus server if we have credentials to do so
Publish-ToOctopusGallery $webAppPackage
Publish-ToOctopusGallery $infrastructurePackage

########################## End .net core version #######################