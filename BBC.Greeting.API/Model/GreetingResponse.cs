﻿namespace BBC.Greeting.API.Model
{
    public class GreetingResponse
    {
        public string Message { get; set; }
    }
}
