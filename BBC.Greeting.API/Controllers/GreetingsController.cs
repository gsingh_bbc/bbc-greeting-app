﻿namespace BBC.Greeting.API.Controllers
{
    using BBC.Greeting.API.Model;
    using Microsoft.AspNetCore.Mvc;
    using System;
    using System.Linq;

    [Route("api/[controller]")]
    [ApiController]
    public class GreetingsController : ControllerBase
    {
        [HttpGet("{name}")]
        public IActionResult Get([FromRoute]string name = null)
        {
            if (string.IsNullOrEmpty(name) || string.IsNullOrWhiteSpace(name) || name.Any(ch => !Char.IsLetterOrDigit(ch)))
            {
                return BadRequest();
            }

            var result = new GreetingResponse
            {
                Message = $"Welcome to BBC Studios {name}"
            };

            return Ok(result);
        }
    }
}