/*
 * These variables will be substituted by variables stored in Octopus Deploy. 
 * They must be included in the plan/apply/destroy steps with the following 
 * CLI argument: 
 *
 *   > terraform plan  -var-file="octopus.tfvars"
 *   > terraform apply -var-file="octopus.tfvars"
 */

environment = "#{Octopus.Environment.ShortName}"
app_version = "#{Octopus.Release.Number}"
application = "#{Octopus.Project.Name}"