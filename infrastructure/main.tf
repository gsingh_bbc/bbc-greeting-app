variable environment {}
variable application {}
variable app_version {}
variable location {

}
variable area_prefix{}
variable sku {
    type        = "map"
    description = "Definition of the dedicated plan to use"
    default = {
      size     = "B1"
      capacity = 1
      tier     = "Basic"
    }
}

variable app_settings {
  type        = "map"
  description = "A key-value pair of App Settings"
  default     = {}
}

variable client_affinity_enabled {
  description = "When true azure will use cookies to route clients to the same instance on every request"
  default = false
}

variable tags {
  type        = "map"
  default     = {}
  description = "Additional tags which should be added to the resource group (env, area, product are already applied)"
}

provider azurerm { }

module "resource_group" {
  source      = "git::ssh://git@bitbucket.org/bbcworldwide/terraform-modules.git//azure/modules/resource-group"
  environment = "${var.environment}"
  application = "${var.application}"
  app_version = "${var.app_version}"
  location    = "${var.location}"
}

# Create a billing plan to run the app inside - this could be dedicated to this app, or shared with other apps
module "app_service_plan" {
  source              = "git::ssh://git@bitbucket.org/bbcworldwide/terraform-modules.git//azure/modules/app-service-plan"
  environment         = "${var.environment}"
  application         = "${var.application}"
  resource_group_name = "${module.resource_group.name}"
  app_version         = "${var.app_version}"
  location            = "${module.resource_group.location}"
  sku                 = "${var.sku}"
}

# create web site deployment infrastructure

locals {
  app_name = "${lower(replace(var.application,"/[ \\._]/","-"))}" # make name lowercase hyphen seperated
  name     = "${var.environment}-${local.app_name}"

  default_tags = {
    env              = "${var.environment}"
    area             = "${var.area_prefix}"
    application      = "${local.app_name}"
    app_version      = "${var.app_version}"
    deploy_timestamp = "${timestamp()}"
  }  
}

resource "azurerm_app_service" "app_service" {
  name                    = "${local.name}"
  location                = "${module.resource_group.location}"
  resource_group_name     = "${module.resource_group.name}"
  app_service_plan_id     = "${module.app_service_plan.id}"
  tags                    = "${merge(local.default_tags, var.tags)}"
  client_affinity_enabled = "${var.client_affinity_enabled}"
  https_only              = true
  
  site_config = {
    always_on                = true
    dotnet_framework_version = "v4.0"
    ftps_state               = "Disabled"
    min_tls_version          = "1.2"

    default_documents = [
      "Default.htm",
      "Default.html",
      "Default.asp",
      "index.htm",
      "index.html",
      "iisstart.htm",
      "default.aspx",
      "index.php",
      "hostingstart.html",
    ]
  }

  app_settings = "${var.app_settings}"
}

output "app_service_name" {
  value = "${azurerm_app_service.app_service.name}"
}

output "app_service_id" {
  value = "${azurerm_app_service.app_service.id}"
}

output "resource_group_name" {
  value = "${module.resource_group.name}"
}

output "default_site_hostname" {
  value = "${azurerm_app_service.app_service.default_site_hostname}"
}

output "outbound_ip_addresses" {
  value = "${azurerm_app_service.app_service.outbound_ip_addresses}"
}

output "site_credential" {
  value = "${azurerm_app_service.app_service.site_credential}"
}

output "identity" {
  value = "${azurerm_app_service.app_service.identity}"
}