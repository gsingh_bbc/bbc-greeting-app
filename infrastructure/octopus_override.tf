
/*  The following section is just a comment if invoked with 
 *  terraform, however after being proceesed by Octopus
 *  Deploy it will be uncommented and substitued. This
 *  allows octopus to apply subscription and backend config 
 */

/* OCTOPUS ONLY *#{if AzureAccount}/#{/if}

  # Apply the AzureRM provider configuration for the Service Principal
  provider "azurerm" {
    subscription_id = "#{AzureAccount.SubscriptionNumber}"
    client_id       = "#{AzureAccount.Client}"
    client_secret   = "#{AzureAccount.Password}"
    tenant_id       = "#{AzureAccount.TenantId}"
  }

  # Apply the backend config for Azure Storage Account
  terraform {
    backend "azurerm" {
      storage_account_name = "#{terraform.backend.storage_account_name}"
      container_name       = "#{terraform.backend.container_name}"
      key                  = "#{terraform.backend.key}"
      access_key           = "#{terraform.backend.access_key}"
    }
  }

/* END OCTOPUS ONLY */