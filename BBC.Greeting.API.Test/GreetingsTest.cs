namespace BBC.Greeting.API.Test
{
    using System.Net;
    using Microsoft.AspNetCore.Mvc;
    using NUnit.Framework;
    using BBC.Greeting.API.Controllers;
    using BBC.Greeting.API.Model;

    public class GreetingsTest
    {        
        [TestCase("Gagan")]
        [TestCase("11111")]
        public void Get_ReturnSuccessMessage_WhenInputIsValid(string name)
        {
            string expectedResult = $"Welcome to BBC Studios {name}";
            var controller = new GreetingsController();
            var okResult = controller.Get(name) as OkObjectResult;

            Assert.AreEqual((int)HttpStatusCode.OK, okResult.StatusCode);

            var result = okResult.Value as GreetingResponse;

            Assert.NotNull(result);
            Assert.AreEqual(expectedResult, result.Message);
        }

        [TestCase(" ")]
        [TestCase("")]
        [TestCase(null)]
        [TestCase("#@$!")]
        public void Get_ReturnBadRequest_WhenInputIsInvalid(string name = null)
        {            
            var controller = new GreetingsController();
            var badResult = controller.Get(name) as BadRequestResult;

            Assert.AreEqual((int)HttpStatusCode.BadRequest, badResult.StatusCode);
        }
    }
}
